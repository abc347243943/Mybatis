package com;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.pojo.User;

public class Test1 {

	public static void main(String[] args) throws IOException {
		String configFileName = "mybatis-config.xml";
		//读取配置文件
		InputStream inputStream = Resources.getResourceAsStream(configFileName);
		//会话工厂
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		//会话
		SqlSession sqlSession = sqlSessionFactory.openSession();
		//查询
		List<User> userList = sqlSession.selectList("user.findList");
		for (User user : userList) {
			//System.out.println(user.getUname()+","+user.getTrueName());
			System.out.println(user);
		}
	}

}
