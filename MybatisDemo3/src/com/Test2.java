package com;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.mapper.AreaMapper;
import com.mapper.UserMapper;
import com.pojo.User;

public class Test2 {

	public static void main(String[] args) throws IOException {
		String configFileName = "mybatis-config.xml";
		//读取配置文件
		InputStream inputStream = Resources.getResourceAsStream(configFileName);
		//会话工厂
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		//会话
		SqlSession sqlSession = sqlSessionFactory.openSession();
		UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
		/*
		//获取记录数据 
		Map<String, Object> searchMap = new HashMap<String, Object>();
		searchMap.put("trueNameLike", "三");
		searchMap.put("userName", "4");
		int count = userMapper.findUserCount(searchMap);
		//searchMap.remove("userName");
		System.out.println("count="+count);
		
		//获取记录列表
		searchMap.put("pageSize", 10);
		searchMap.put("currentPageSize", 0);
		List<User> pageList = userMapper.pageList(searchMap);
		for (User user : pageList) {
			System.out.println(user);
		}
		*/
		//增加
		User user = new User();
		user.setUname("test092902");
		user.setTrueName("测试人员092902");
		user.setUpwd("123456");
		int result = userMapper.saveUser(user);
		sqlSession.commit();
		if (result > 0 ) {
			System.out.println("保存成功");
		}
		else {
			System.out.println("保存成功");
		}
		/*
		//修改
		User user = new User();
		user.setUserNameParam("test092901");
		user.setTrueName("测试人员092901");
		user.setUpwd("12345688");
		user.setUid(32);
		int result = userMapper.updateUser(user);
		sqlSession.commit();
		if (result > 0 ) {
			System.out.println("修改成功");
		}
		else {
			System.out.println("修改成功");
		}
		*/
		/*
		//删除
		int result = userMapper.deleteUser(32);
		sqlSession.commit();
		if (result > 0 ) {
			System.out.println("删除成功");
		}
		else {
			System.out.println("删除成功");
		}
		*/
		/*
		//获取地区数据
		AreaMapper areaMapper = sqlSession.getMapper(AreaMapper.class);
		List<Map<String, Object>> areaList = areaMapper.findAreaAllList();
		for (Map<String, Object> map : areaList) {
			System.out.println(map);
		}
		*/
	}

}
