package com.dao;

import java.util.List;
import java.util.Map;

import com.pojo.User;

public interface UserDaoMapper {
	public List<User> findUserAllList();
	
	public List<User> findListByTrueName(String trueName);

	public User findListById(int uid);

	public int saveUser(User user);

	public int updateUser(User user);

	public int deleteUser(int i);

	public List<User> findPageList(User paramUser);

	public List<Map<String, Object>> findDicsList(Map<String, Object> searchMap);
}
