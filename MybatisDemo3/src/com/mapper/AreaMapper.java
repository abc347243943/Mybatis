package com.mapper;

import java.util.List;
import java.util.Map;

import com.pojo.User;

public interface AreaMapper {
	public List<Map<String, Object>> findAreaAllList();
	
}
