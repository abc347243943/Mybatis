package com.mapper;

import java.util.List;
import java.util.Map;

import com.pojo.User;

public interface UserMapper {
	/**
	 * 获取记录数
	 * @param searchMap
	 * @return
	 */
	public int findUserCount(Map<String, Object> searchMap);

	/**
	 * 获取列表数据
	 * @param searchMap
	 * @return
	 */
	public List<User> pageList(Map<String, Object> searchMap);

	/**
	 * 保存用户
	 * @param user
	 * @return
	 */
	public int saveUser(User user);

	/**
	 * 修改用户
	 * @param user
	 * @return
	 */
	public int updateUser(User user);

	/**
	 * 删除用户
	 * @param userId
	 * @return
	 */
	public int deleteUser(int userId);
}
