package com.pojo;

import lombok.Data;

@Data
public class User {
	private int uid;
	private String uname;
	private String userNameParam;
	private String trueName;
	private String upwd;
	private String orderByValue;
	private String sex;
	private String oprDate;
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.uid+","+this.trueName+","+this.uname
				+","+this.oprDate;
	}
}
